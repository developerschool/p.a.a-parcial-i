/*
Atividade

Desenvolver um algoritmo em linguagem C, que satisfaça os segulong intes critérios:
- Criar uma função para gerar valores aleatórios (hand);
*/

//Bibliotecas - Cabeçalhos - Funções e procedimentos - Informações de Continuidade
/**Bibliotecas*/

//Sistema
#include <stdio.h> // Biblioteca I/O
#include <stdlib.h> // Biblioteca para Manipulação de Arquivos
#include <string.h> // Biblioteca para Manipulação de Strings
#include <locale.h> //Biblioteca para permitir acentuação gráfica
#include <time.h> // Biblioteca para manipulação de Tempo (Timer - Horário)
//#include <windows.h> // Biblioteca para permitir utilização da função Sleep();

//Específicas
#define clear system("cls || clear"); // Definições para limpar a tela.
#define clear_buffer setbuf(stdin, NULL); // Limpar buffer.
#define pause getchar(); // Pausar a tela.

//Diversas
#define delay1x Sleep(2000); // Delay quando imprimir algo.
#define delay2x Sleep(1000); // Delay quando for impremir algo.
#define MaxTam 100000 // Quantidade de cadastros disponíveis

/* Estrura / Registros */
struct TipoItem {
  long int valor;

};

typedef struct TipoDados {
  struct TipoItem Item[MaxTam];
  long int ult,
      cadastrado,
      visualizado;
}tDADOS;
/*............................*/

/*Protótipos de funções referente a estrutura de dados*/
tDADOS *fCriarFila(); // Função para criar a estrutura de dados do tipo Fila.
void fCadastrar(tDADOS *Fila, long int op); // Função para cadastrar dados.
long int fFilaCheia(tDADOS *Fila); // Função para verificar se a estrutura está cheia.
long int fFilaVazia(tDADOS *Fila); // Função para verificar se a estrutura está vazia.
/*............................................................................*/

/*Protótipos de funções*/
void fCenterString(); // Função para Centralizar uma String.
void fMenu();
void fTeste(tDADOS *Fila); // Função para listar, ordenar e exibir dados.
void fExibir(tDADOS *Fila); // Função para exibir tudo

//Funções de Ordenaçãõ
void fOrdenacao(tDADOS *Fila, long int op); // Função para Ordenar a Estrutura de dados e chamar a função exibir
void fOrdenacaoQuicksort(tDADOS *Fila, long int inicio, long int fim); // Função para Ordenação do tipo Quick Sort
long int fParticionar(tDADOS *Fila, long int inicio, long int fim); //Função para particionar por idade e retornar pivôs
/*............................................................................*/

/*Protótipos de funções referente a Manipulação de Arquivos*/
void fWrite_File(tDADOS *Fila); //Fun��o para escrever no arquivo (Cadastros dos clientes salvos em um arquivo)
void fOpen_File(tDADOS *Fila); //Fun��o ler no arquivo (Ler os cadastros dos clientes salvos em um arquivo)
void fSalvar_Contador(tDADOS *Fila); //Fun��o para salvar contador
void fOpen_Contador(tDADOS *Fila); //Fun��o para Abrir contador

/*............................................................................*/
