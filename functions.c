#include "library.h"

void fMenu() {

  tDADOS *Fila = fCriarFila();
  //fOpen_Contador(Fila);
  //fOpen_File(Fila);

  long int x = 1;
  while(x) {//Enquanto x for verdadeiro ...
    clear_buffer;
    clear;
    puts("\n\n");
    printf("\n------------------------------------------------------------------------------\n\n");
    printf("\t\t\tORDENAÇÃO DE SEQUENCIAS NUMÉRICAS - \n\n");
    printf("\t\t\t4 - [1.000.000]\n");
    printf("\t\t\t3 - [100.000]\n");
    printf("\t\t\t2 - [10.000]\n");
    printf("\t\t\t1 - [100]\n");
    printf("\t\t\t0 - Sair\n");
    printf("\n------------------------------------------------------------------------------\n");

    long int op;
    scanf("%ld", &op);

    switch(op) {
      case 0: exit(0);
      case 1: fCadastrar(Fila, 100); break;
      case 2: fCadastrar(Fila, 10000); break;
      case 3: fCadastrar(Fila, 100000); break;
      case 4: fCadastrar(Fila, 1000000); break;
    }
  }
}

// Função para Centralizar uma String
void fCenterString(char *string) {
  //Lê-se: Imprima toda a string ("%*s"), (Na posição tal = (antes, string, depois))
  //Em antes e depois preencha com espaços ("").
  printf("%*s", 40 + strlen(string)/2, string, 40 - strlen(string)/2, "");
}

// Função para criar a estrutura de dados do tipo Fila
tDADOS* fCriarFila() {
  /* Crie uma estrutura (do tipo tDADOS ) que vai ter como início (e orientação
     de posicionamento) o ponteiro *ptr_Fila do tamanho de (1 , espaço do
     tamanho da estrutura  tDADOS) */
  tDADOS *ptr_Fila = (tDADOS *) calloc(4, sizeof(tDADOS) * 100);

  /* O ponteiro "ptr_Fila que está apontando para a variável "ult" vai receber
     o valor "-1" representando que a fica está fazia e a próxima posição
     disponível é a Item[0]*/
  ptr_Fila->ult = -1;
  return ptr_Fila;
}

// Função para Cadastrar dados
void fCadastrar(tDADOS *Fila, long int op) {
  if(fFilaCheia(Fila)) { // Se(verdadeiro) - "fila cheia" for verdadeiro...
    printf("Espaço Insuficiente!");
  }
  else {
    clear_buffer; // Limpar buffer do teclaro
    // Salve na Estrurura dados que tiver a posição Estrutura[Fila->ult] ...
    long int i;
    for(i = 0; i < op; i++) {
      Fila->ult++;
      Fila->Item[i].valor = rand() % op; // Vai receber números aleatório entre 0 e "op".
      Fila->cadastrado++;
    }

    //fWrite_File(Fila);//Salvar no arquivo o cadastro
    //fSalvar_Contador(Fila);//Salvar o Contador(Quantidade de registros) cadastro
    fTeste(Fila);
  }
}

// Função para verificar se a estrutura está cheia
long int fFilaCheia(tDADOS *Fila) {
  /* Retorne o resultado da seguinte condição:
     Se a ultima posição da estrutura for a ultima... return 1 para verdadeiro e
     0 para falso */
  return (Fila->ult == MaxTam - 1);
}

// Função para verificar se a estrutura está vazia.
long int fFilaVazia(tDADOS *Fila) {
  /* Mesmo sentido que a condição da "fFilaCheia", porém retorne verdadeiro(1)
     ou falso(0) se a posição é a inicial.
  se a posição  */
  return (Fila->ult == -1);
}

// Função para consultar cadastros já realizados
void fTeste(tDADOS *Fila) {
  while(1) {
    clear_buffer;
    clear;
    puts("\n\n");
    printf("\n------------------------------------------------------------------------------\n\n");
    printf("\t\t\tSELECIONE QUAL TIPO DE ORDENAÇÃO\n\n");
    printf("\t\t\t4 - Quick Sort\n");
    printf("\t\t\t3 - ...\n");
    printf("\t\t\t2 - ...\n");
    printf("\t\t\t1 - Voltar ao menu principal\n");
    printf("\t\t\t0 - Sair\n");
    printf("\n------------------------------------------------------------------------------\n");
    long int op;
    scanf("%ld", &op);

    switch(op) {
      case 0:exit(0);
      case 1:fMenu();
      case 4:fOrdenacao(Fila, 3); break;
    }
    fMenu();
  }
}

// Função para exibir tudo
void fExibir(tDADOS *Fila) {
  clear_buffer;
  if(fFilaVazia(Fila)) {
    puts("\n\n\n");
    fCenterString("Dados não encontrados!");
  }
  else {
    printf("\n------------------------------------------------------------------------------");
    puts("\n");
    long int i;
    for(i = 0; i <= Fila->ult; i++) {
      printf("[%i] , ", Fila->Item[i].valor);
      Fila->visualizado++;
    }
    puts("");
    printf("\n------------------------------------------------------------------------------\n");
  }
}

// Função para Ordenar a Estrutura de dados e chamar a função exibir
void fOrdenacao(tDADOS *Fila, long int op) {
  long int inicio, fim;
  inicio = 0;
  fim = Fila->ult;

  clear;
  fExibir(Fila);

  if(op == 3) {
    fOrdenacaoQuicksort(Fila, inicio, fim);
  }

  fExibir(Fila);
  printf("\nValores Cadastrados: %i"
         "\nValores Visualizados: %i", Fila->cadastrado, Fila->visualizado);

  Fila->cadastrado = 0;
  Fila->visualizado = 0;
  pause;
  //Gravar no arquivo a ordena��o
  //fWrite_File(Fila);//Salvar no arquivo o cadastro
}

// Função para Ordenação do tipo Quick Sort
void fOrdenacaoQuicksort(tDADOS *Fila, long int inicio, long int fim) {
  long int pivo;

  if(inicio < fim) {
    pivo = fParticionar(Fila, inicio, fim);
    fOrdenacaoQuicksort(Fila, inicio, pivo - 1);
    fOrdenacaoQuicksort(Fila, pivo + 1, fim);
  }
}

long int fParticionar(tDADOS *Fila, long int inicio, long int fim) {
  long int pivo, i, j;
  tDADOS temp;

  i = inicio;
  j = fim + 1;
  pivo = Fila->Item[i].valor;

  while(1) {
    do { ++i; }while(Fila->Item[i].valor <= pivo && i <= fim);
    do { --j; }while(Fila->Item[j].valor > pivo);

    if(i >= j) {
      break;
    }else {
      temp.Item[0] = Fila->Item[i];
      Fila->Item[i] = Fila->Item[j];
      Fila->Item[j] = temp.Item[0];
    }
  }

  temp.Item[0] = Fila->Item[inicio];
  Fila->Item[inicio] = Fila->Item[j];
  Fila->Item[j] = temp.Item[0];
  return j;
}

//Funçãoo para escrever no arquivo (Cadastros dos clientes salvos em um arquivo)
void fWrite_File(tDADOS *Fila) {

    FILE *ptr_struct;
    ptr_struct = fopen("Cadastro.bin", "w+b");

    if(!ptr_struct) {
        printf("Erro na abertura do arquivo!");
        exit(1);
    }
    //Escrever no arquivo(Pegar o endere�o da vari�vel, Tamanho da vari�vel, Quantidade de Vari�veis, ponteiro do Arquivo)
    fwrite(&Fila->Item, sizeof(struct TipoItem),Fila->ult+1, ptr_struct);
    fclose(ptr_struct);
}

//Fun��o ler no arquivo (Ler os cadastros dos clientes salvos em um arquivo)
void fOpen_File(tDADOS *Fila) {

    FILE *ptr_struct;
    ptr_struct = fopen("Cadastro.bin", "a+b");//Ler + bin�rio

    if(!ptr_struct) {
        printf("Erro na abertura do arquivo!");
        exit(1);
    }
    //Ler no arquivo(Pegar o endere�o da vari�vel, Tamanho da vari�vel, Quantidade de Vari�veis, ponteiro do Arquivo)
    fread(&Fila->Item, sizeof(struct TipoItem), Fila->ult+1, ptr_struct);
    fclose(ptr_struct);
}

//Fun��o para salvar contador
void fSalvar_Contador(tDADOS *Fila) {

    FILE *ptr_contador;
    ptr_contador = fopen("Contador.bin", "w + b");

    if(!ptr_contador) {
        printf("Erro na abertura do arquivo!");
        exit(1);
    }

    fwrite(&Fila->ult, sizeof(int), 1, ptr_contador);
    fclose(ptr_contador);
}

//Fun��o para Abrir contador
void fOpen_Contador(tDADOS *Fila) {

    FILE *ptr_contador;
    ptr_contador = fopen("Contador.bin", "a + b");

    if(!ptr_contador) {
        printf("Erro na abertura do arquivo!");
        exit(1);
    }

    fread(&Fila->ult, sizeof(int), 1, ptr_contador);
    fclose(ptr_contador);
}
/**/
